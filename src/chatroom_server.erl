-module(chatroom_server).
-behaviour(gen_server).

-export([start_link/0]).
-export([init/1]).
-export([handle_cast/2, handle_info/2, handle_call/3]).
-export([join/2, list_users/1]).

-record(chatroom, {attendees=[]}).

start_link() ->
  gen_server:start_link(?MODULE,[],[]).

init(_Args) ->
  {ok, #chatroom{}}.

broadcast_message(Pid, Message) ->
  gen_server:cast(Pid, {broadcast, Message}).

join(Pid, User) ->
  gen_server:cast(Pid, {join_room, User}).

leave(Pid, User) ->
  gen_server:cast(Pid, {leave_room, User}).

list_users(Pid) ->
  gen_server:call(Pid, {list_users}).

handle_call({list_users}, _, State) ->
  {reply, State, State}.

handle_cast({broadcast, Message}, #chatroom{attendees= Attendees} = State) ->
  lists:foreach(fun({_, User})-> User ! {message, Message} end, Attendees),
  {noreply, State};
handle_cast({join_room, {Name, UserPid} = User}, #chatroom{attendees= Attendees} = State) ->
  erlang:monitor(process, UserPid),
  send_all(Attendees, #{<<"topic">> => <<"new_user">>, <<"name">> => Name }),
  {noreply, State#chatroom{attendees= [User | State#chatroom.attendees]}};
handle_cast({leave_room, User}, State) ->
  {noreply, State#chatroom{attendees= remove_user(State#chatroom.attendees, User) } }.

handle_info({'DOWN', Ref, process, Pid2, Reason}, #chatroom{attendees= Attendees} = State) ->
  {noreply, State#chatroom{attendees= remove_user(Attendees, Pid2)} }.

remove_user(Attendees, User) ->
  lists:filter(fun({_, FromList})-> FromList /= User end, Attendees).

send_all(Attendees, Message) ->
  lists:foreach(fun({_, User})-> User ! {message, Message} end, Attendees).