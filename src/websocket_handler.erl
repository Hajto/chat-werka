-module(websocket_handler).

-export([init/2]).
-export([websocket_init/1]).
-export([websocket_handle/2]).
-export([websocket_info/2]).

init(Req, Opts) ->
	{cowboy_websocket, Req, Opts}.

websocket_init(State) ->
	{ok, nil}.

websocket_handle({text, Msg}, State) ->
	io:fwrite("~nSTATE:~p~n", [State]),
	#{ <<"topic">> := Topic, <<"payload">> := Payload} = jsone:decode(Msg),
	{Response, NewState} = handle_request(Topic, Payload, State),
	Encoded = jsone:encode(Response),
	{reply, {text, Encoded}, NewState};
websocket_handle(_Data, State) ->
	{ok, State}.

websocket_info({timeout, _Ref, Msg}, State) ->
	{reply, {text, Msg}, State};
websocket_info({message, Message}, State) ->
	{reply, {text, jsone:encode(wrap_response(#{ <<"message">> => Message}))}, State}; 
websocket_info(_Info, State) ->
	{ok, State}.


handle_request(<<"get_users_in_chatrrom">>, #{<<"chatroom">> := Channel}, State) ->
	Result = userets:list_users_in_chatroom(Channel),
	{wrap_response(#{<<"users">> => Result, <<"chatroom">> => Channel}), State};
handle_request(<<"set_name">>, #{<<"name">> := Name}, State) ->
	Result = userets:register_name(Name, self()),
	NewState = case Result of
		true -> Name;
		_ -> nil
	end,
	{wrap_response(#{<<"result">> => Result}), NewState};
handle_request(<<"get_users">>, _, State) ->
	Users = userets:list_users(),
	UserNames = lists:map(fun({Name, _}) -> Name end, Users),
	{ wrap_response(#{<<"users">> => UserNames}), State };
handle_request(<<"get_channels">>, _, State) ->
	Channels = chatroom:list_names(),
	{wrap_response(#{"chatrooms" => Channels}), State};
handle_request(<<"join">>, #{<<"channel">> := Channel}, State) ->
	Result = case chatroom:join(Channel, {State, self()} ) of
		ok -> wrap_response(#{<<"message">> => <<"Channel joined!">>})
	end,
	{Result, State};
handle_request(<<"broadcast">>, #{ <<"message">> := Msg, <<"channel">> := Topic}, State) ->
	chatroom:send_message(Topic, Msg),
	{ wrap_response(#{<<"notify">> => <<"Message sent!">>	}), State}.

terminate(Reason, PartialReq, nil) ->
	ok;
terminate(Reason, PartialReq, State) ->
	userets:remove_user(State),
	ok.
wrap_response(Response) ->
	#{<<"payload">> => Response}.