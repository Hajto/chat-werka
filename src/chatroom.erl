-module(chatroom).
-export([join/2, create/1, send_message/2, list_names/0, list_users_in_chatroom/1]).

create(Name) ->
  {ok, Pid} = supervisor:start_child(chatroom_sup, []),
  syn:join(chatroom, Pid, Name),
  Pid.

join(Name, Username) ->
  Pid = case find_chat_room(Name) of
    false -> create(Name);
    {RPid, _ } -> RPid
  end,
  chatroom_server:join(Pid, Username).

send_message(Name, Message) ->
  case find_chat_room(Name) of
    false -> {error, noproces};
    {RPid, _ } ->
      gen_server:cast(RPid, {broadcast, Message}),
    {ok, RPid}
  end.

list_users_in_chatroom(Name) ->
  case find_chat_room(Name) of
    false -> {error, noproces};
    {RPid, _} ->
      chatroom_server:list_users(RPid)
  end.

find_chat_room(Name) ->
  Members = list_chatrooms(),
  lists:keyfind(Name,2,Members).

list_chatrooms() ->
  syn:get_members(chatroom, with_meta).

list_names() ->
  lists:map(fun({_, Name}) -> Name end, list_chatrooms()).