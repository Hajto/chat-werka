-module(chatroom_sup).
-behaviour(supervisor).

-export([start_link/0]).
-export([init/1]).

start_link() ->
  supervisor:start_link({local, ?MODULE}, ?MODULE, [{name, ?MODULE}]).

init(_Args) ->
  SupFlags = #{strategy => simple_one_for_one,
                intensity => 0,
                period => 1},
  ChildSpecs = [#{id => chatroom,
                  start => {chatroom_server, start_link, []},
                  shutdown => brutal_kill}],
  io:fwrite("~nStarted chatroom supervisor ~n"),
  {ok, {SupFlags, ChildSpecs}}.