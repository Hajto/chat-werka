-module(userets).
-export([register_name/2, get_user_pid/1, remove_user/1, list_users/0]).

register_name(Name, Pid) ->
  ets:insert_new(users, {Name, Pid}).

get_user_pid(Name) ->
  ets:lookup(users, Name).

remove_user(Name) ->
  ets:delete(users, Name).

list_users() ->
  ets:tab2list(users).
