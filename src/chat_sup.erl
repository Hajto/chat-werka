%%%-------------------------------------------------------------------
%% @doc chat top level supervisor.
%% @end
%%%-------------------------------------------------------------------

-module(chat_sup).

-behaviour(supervisor).

%% API
-export([start_link/0]).

%% Supervisor callbacks
-export([init/1]).

-define(SERVER, ?MODULE).

%%====================================================================
%% API functions
%%====================================================================

start_link() ->
    supervisor:start_link({local, ?SERVER}, ?MODULE, []).

%%====================================================================
%% Supervisor callbacks
%%====================================================================

%% Child :: {Id,StartFunc,Restart,Shutdown,Type,Modules}
init([]) ->
    Result = ets:new(users, [set, public, named_table]),
    io:fwrite("ETS creation: ~p~n",[Result]),
    {ok, { {one_for_one, 0, 1}, [#{
        id => chatroom_sup,
        start =>{chatroom_sup, start_link, []}
    }]} }.

%%====================================================================
%% Internal functions
%%====================================================================
