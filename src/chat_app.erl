%%%-------------------------------------------------------------------
%% @doc chat public API
%% @end
%%%-------------------------------------------------------------------

-module(chat_app).

-behaviour(application).

%% Application callbacks
-export([start/2, stop/1]).

%%====================================================================
%% API
%%====================================================================

start(_StartType, _StartArgs) ->
    start_cowboy(),
    %% connect to nodes
    %% connect_nodes(),
    %% init syn
    syn:init(),
    chat_sup:start_link().

%%--------------------------------------------------------------------
stop(_State) ->
    ok.

%%====================================================================
%% Internal functions
%%====================================================================

start_cowboy() ->
    Dispatch = cowboy_router:compile([
        {'_', [
            {"/", cowboy_static, {priv_file, chat, "index.html"}},
            {"/websocket", websocket_handler, []},
            {"/static/[...]", cowboy_static, {priv_dir, websocket, "static"}}
        ]}
    ]),
    {ok, CowboyPid} = cowboy:start_clear(http, [{port, 8080}], #{
        env => #{dispatch => Dispatch}
    }),
    CowboyPid.